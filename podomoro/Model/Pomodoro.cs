﻿using System;
using System.Collections.Generic;
using System.Text;

namespace podomoro.Model
{
    public class Pomodoro
    {
        //public List<int> iterationsTime = new List<int> { 25, 5, 25, 5, 25, 5, 25, 15 };
        public List<int> iterationsTime = new List<int> { 1,1 };
        public TimeSpan time = new TimeSpan(0, 1, 0);
        public int iteration = 0;

        public Pomodoro()
        {
          
        }

       public bool isEnded()
        {
            return this.iteration == this.iterationsTime.Count;
        }

        public void stopPomodoro()
        {
            this.iteration = this.iterationsTime.Count;
        }

        public List<int> IterationsTime { get => iterationsTime; set => iterationsTime = value; }
        public TimeSpan Time { get => time; set => time = value; }
        public int Iteration { get => iteration; set => iteration = value; }

    }
}
