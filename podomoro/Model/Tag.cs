﻿using System;
using System.Collections.Generic;
using System.Text;

namespace podomoro.Model
{
    class Tag
    {
        private string name;

        Tag(string name)
        {
            this.name = name;
        }

        public string Name { get => name; set => name = value; }
    }
}
