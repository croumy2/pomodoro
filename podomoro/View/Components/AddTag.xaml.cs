﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace podomoro.View.Components
{
    /// <summary>
    /// Logique d'interaction pour AggTad.xaml
    /// </summary>
    public partial class AddTag : Window
    {
        public AddTag()
        {
            InitializeComponent();
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            using (var db = new EntityPomodoro())
            {
                Entity.Tag tag = new Entity.Tag();
                tag.name = addTagText.Text;

                db.Tags.Add(tag);
                db.SaveChanges();
            }
            this.Close();
        }
    }
}
