﻿using podomoro.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace podomoro.View.Components
{
    /// <summary>
    /// Logique d'interaction pour AddDescriptionPomodoro.xaml
    /// </summary>
    public partial class AddDescriptionPomodoro : Window
    {
        User user;
        public AddDescriptionPomodoro(User user)
        {
            this.user = user;
            InitializeComponent();
        }

        private void validateButton_Click(object sender, RoutedEventArgs e)
        {
            using (var db = new EntityPomodoro())
            {
                User currentUser = db.Users.Where(u => u.name == this.user.name).First();

                Entity.Pomodoro currentPomodoro = new Entity.Pomodoro();
                currentPomodoro.user = currentUser;
                currentPomodoro.date = DateTime.Now;
                currentPomodoro.Description = descriptionText.Text;

                db.Pomodoros.Add(currentPomodoro);
                db.SaveChanges();
            }

            this.Close();
        }
    }
}
