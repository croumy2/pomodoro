﻿using podomoro.Model;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;


namespace podomoro
{
    /// <summary>
    /// Logique d'interaction pour Page1.xaml
    /// </summary>
    public partial class LoginPage : Page
    {
        private readonly Window _mainWindow;
        public LoginPage(Window mainWindow)
        {
            InitializeComponent();
            _mainWindow = mainWindow;
        }

        private void loginButton_Click(object sender, RoutedEventArgs e)
        {
            User user;
            string name = loginTextBox.Text;

            using (var db = new EntityPomodoro())
            {
                List<User> users = db.Users
                    .Where(user => user.name == name)
                    .ToList();

                //If user doesn't exist, save it to db
                if(users.Count == 0)
                {
                    user = new User();
                    user.name = name;
                    db.Users.Add(user);
                    db.SaveChanges();
                } else
                {
                    user = users.First();
                }

            }

            Frame mainFrame = _mainWindow.FindName("Main") as Frame;
            mainFrame.Navigate(new PomodoroPage(this._mainWindow, user));
        }
    }
}
