﻿using podomoro;
using podomoro.Model;
using podomoro.View.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace podomoro
{
    /// <summary>
    /// Logique d'interaction pour Pomodoro.xaml
    /// </summary>
    public partial class PomodoroPage : Page
    {
        private readonly Window _mainWindow;

        private DispatcherTimer dispatcherTimer = new DispatcherTimer();
        private bool isStarted = false;
        private Pomodoro pomodoro = new Pomodoro();
        private User user;

        public PomodoroPage(Window mainWindow, User user)
        {
            this._mainWindow = mainWindow;
            this.user = user;

            InitializeComponent();
            dispatcherTimer.Interval = TimeSpan.FromSeconds(1);
            dispatcherTimer.Tick += dispatcherTimer_Tick;

            timer.Content = $"{pomodoro.Time.Minutes} : {pomodoro.Time.Seconds}";
        }

        private void launchButton_Click(object sender, RoutedEventArgs e)
        {
            stopButtonGroup.Visibility = Visibility.Visible;

            if (!this.isStarted)
            {
                this.isStarted = true;
                playIcon.Visibility = Visibility.Hidden;
                pauseIcon.Visibility = Visibility.Visible;
                dispatcherTimer.Start();
            }
            else
            {
                this.isStarted = false;
                playIcon.Visibility = Visibility.Visible;
                pauseIcon.Visibility = Visibility.Hidden;
                dispatcherTimer.Stop();
            }
        }

        public void showDescriptionWindow()
        {
            AddDescriptionPomodoro description = new AddDescriptionPomodoro(this.user);
            description.Show();

            this.isStarted = false;

            //Use a new pomodoro
            this.pomodoro = new Pomodoro();
            timer.Content = $"{pomodoro.Time.Minutes} : {pomodoro.Time.Seconds}";
            statusLabel.Content = "Démarrer un podomoro";
            playIcon.Visibility = Visibility.Visible;
            pauseIcon.Visibility = Visibility.Hidden;
            stopButtonGroup.Visibility = Visibility.Hidden;
        }

        public void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            statusLabel.Content = pomodoro.iteration % 2 == 0 ? "En plein travail" : "Prenez une pause";

            //End of an iteration
            if (pomodoro.Time.Seconds == 0 && pomodoro.Time.Minutes == 0)
            {
                dispatcherTimer.Stop();
                MessageBox.Show("Itération terminée");
                pomodoro.Iteration++;

                //End Of the entire Pomodoro Session
                if (pomodoro.isEnded())
                {
                    this.showDescriptionWindow();
                }
                else
                {
                    pomodoro.Time = new TimeSpan(0, pomodoro.IterationsTime[pomodoro.Iteration], 0);
                    dispatcherTimer.Start();
                }
            }
            else
            {
                pomodoro.Time = pomodoro.Time.Subtract(TimeSpan.FromSeconds(1));
                timer.Content = $"{pomodoro.Time.Minutes} : {pomodoro.Time.Seconds}";
            }

        }

        private void historyButton_Click(object sender, RoutedEventArgs e)
        {
            Frame mainFrame = _mainWindow.FindName("Main") as Frame;
            mainFrame.Navigate(new History(this.user));
        }

        private void stopButton_Click(object sender, RoutedEventArgs e)
        {
            dispatcherTimer.Stop();
            this.showDescriptionWindow();
        }
    }
}
