﻿using podomoro.Model;
using podomoro.View.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace podomoro
{
    /// <summary>
    /// Logique d'interaction pour History.xaml
    /// </summary>
    public partial class History : Page
    {
        private User user;
        public History(User user)
        {
            this.user = user;

            InitializeComponent();
            List<Entity.Pomodoro> pomodoros = new List<Entity.Pomodoro> { };

            using (var db = new EntityPomodoro())
            {
                User currentUser = db.Users.Where(u => u.name == this.user.name).First();

                pomodoros = db.Pomodoros.Where(pomodoro => pomodoro.user == currentUser).ToList();
                List<Entity.Tag> tags = db.Tags.ToList();
            }


            foreach (Entity.Pomodoro pomodoro in pomodoros)
            {
                
                listPomodoro.Items.Add(pomodoro);
            }
        }

        private void backButton_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.GoBack();
        }

        private void addTagButton_Click(object sender, RoutedEventArgs e)
        {
            AddTag addTagWindow = new AddTag();
            addTagWindow.Show();
        }
    }
}
