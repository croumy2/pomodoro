﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace podomoro.Migrations
{
    public partial class TagsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "tagidTag",
                table: "Pomodoros",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Tags",
                columns: table => new
                {
                    idTag = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tags", x => x.idTag);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Pomodoros_tagidTag",
                table: "Pomodoros",
                column: "tagidTag");

            migrationBuilder.AddForeignKey(
                name: "FK_Pomodoros_Tags_tagidTag",
                table: "Pomodoros",
                column: "tagidTag",
                principalTable: "Tags",
                principalColumn: "idTag",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Pomodoros_Tags_tagidTag",
                table: "Pomodoros");

            migrationBuilder.DropTable(
                name: "Tags");

            migrationBuilder.DropIndex(
                name: "IX_Pomodoros_tagidTag",
                table: "Pomodoros");

            migrationBuilder.DropColumn(
                name: "tagidTag",
                table: "Pomodoros");
        }
    }
}
