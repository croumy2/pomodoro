﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace podomoro.Migrations
{
    public partial class DescriptionPomodoro : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Pomodoros",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "Pomodoros");
        }
    }
}
