﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using podomoro.Entity;
using podomoro.Model;

namespace podomoro
{
    public class EntityPomodoro : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Entity.Pomodoro> Pomodoros { get; set;}
        public DbSet<Entity.Tag> Tags { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\cleme\Documents\pomodoro.mdf;Integrated Security=True;Connect Timeout=30");
        }
    }

}
