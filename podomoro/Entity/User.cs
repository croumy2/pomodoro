﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace podomoro.Model
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int idUser { get; set; }
       
        public string name { get; set; }
        public List<Entity.Pomodoro> pomodoros { get; set; }


        public override string ToString()
        {
            return $"name: {this.name}";
        }

        /*public User(int idUser, String name) {
            this.idUser = idUser;
            this.name = name;
        }*/

        
    }
}
