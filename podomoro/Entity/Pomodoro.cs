﻿using podomoro.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace podomoro.Entity
{
    public class Pomodoro
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int idPomodoro { get; set; }

        public DateTime date { get; set; }
        public User user { get; set; }
        public string Description { get; set; }
        public Tag tag { get; set; }
    }
}
