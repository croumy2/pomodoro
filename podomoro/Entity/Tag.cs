﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace podomoro.Entity
{
    public class Tag
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int idTag { get; set; }

        public string name { get; set; }
        public List<Entity.Pomodoro> pomodoros { get; set; }

        public override string ToString()
        {
            return $"nameTag: {this.name}";
        }
    }
}
