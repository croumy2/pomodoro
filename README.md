# Projet pomodoro C# / WPF

L'objectif de ce TP est de réaliser un pomodoro et de pouvoir y ajouter une description en fin de session.

## Fonctionnalités

- Connexion avec son nom
- Lancement d'un pomodoro
- Consulter ses pomodoro déjà réalisés
- Ajout d'une description en fin de session
- Pouvoir arrêter son pomodoro avant la fin du temps
- Ajouter des tags à ses session (Pas fait (seulement la possibilité d'ajouter des tags à la liste des tags))


## Ce que j'ai appris (difficultés rencontrées)

- Découvert de WPF (première fois que je réalise une application avec ce framework)
- Utilisation du framework Entity
- Utilisation du XAML pour réaliser la partie front
- Navigation entre les pages d'une fenêtre
- Structure d'un projet WPF